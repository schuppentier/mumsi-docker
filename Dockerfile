FROM ubuntu:latest

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install libboost-all-dev liblog4cpp5-dev libpjsua2 cmake pkg-config git gcc g++ libssl-dev libprotobuf-dev protobuf-compiler libopus-dev
RUN apt-get -y install libpjsip2 libpjmedia2 libpjproject-dev
RUN apt-get -y install libasound2
RUN git clone https://github.com/mrscotty/mumlib.git /mumlib
RUN cd /mumlib && mkdir build && cd build && cmake .. && make
RUN git clone https://github.com/mrscotty/mumsi.git /mumsi
RUN cd /mumsi && mkdir build && cd build && cmake .. && make

ADD config.ini /config.ini
